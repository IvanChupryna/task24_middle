﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Repositories;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class CommentController : Controller
    {
        UnitOfWork unitOfWork;
        public CommentController()
        {
            unitOfWork = new UnitOfWork();
        }
        // GET: Comment
        public ActionResult ShowComments()
        {
            return View(unitOfWork.CommentRepository.GetAllWithRelated());
        }

        [HttpPost]
        public ActionResult ShowComments(FormCollection form)
        {
            string name = form.Get("username");
            User currentUser = unitOfWork.UserRepository.GetByName(name);
            if(currentUser == null)
            {
                currentUser = new User() { Name = name };
                unitOfWork.UserRepository.Add(currentUser);
                unitOfWork.Save();
            }

            string comment_text = form.Get("comment_text");
            Comment currentComment = new Comment()
            {
                Text = comment_text,
                DateTimeCommented = DateTime.Now,
                UserId = unitOfWork.UserRepository.GetByName(name).Id
            };

            unitOfWork.CommentRepository.Add(currentComment);
            unitOfWork.Save();
            return RedirectToAction("ShowComments");
        }
    }
}