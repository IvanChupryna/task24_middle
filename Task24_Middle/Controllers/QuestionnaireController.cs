﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Models;
using Task24_Middle.Repositories;

namespace Task24_Middle.Controllers
{
    public class QuestionnaireController : Controller
    {
        UnitOfWork unitOfWork;

        public QuestionnaireController()
        {
            unitOfWork = new UnitOfWork();
        }

        [HttpGet]
        public ActionResult ShowQuestions()
        {
            return View(unitOfWork.QuestionRepository.GetAllWithRelated());
        }

        [HttpPost]
        public ActionResult ShowResult(FormCollection form)
        {
            Questionnaire questionnaire = new Questionnaire()
            {
                FilledDateTime = DateTime.Now
            };
            unitOfWork.QuestionnaireRepository.Add(questionnaire);
            unitOfWork.Save();

            User user = unitOfWork.UserRepository.GetByName(form["username"]);
            if (form["username"] == String.Empty)
            {
                ModelState.AddModelError("username", "Please enter username");
            }
            if (user == null)
            {
                user = new User()
                {
                    Name = form["username"]
                };
                unitOfWork.UserRepository.Add(user);
                unitOfWork.Save();
            }

            foreach (var key in form.AllKeys)
            {
                if (key == "username") continue;
                Answer currentAnswer = new Answer()
                {
                    AnswerText = form[key],
                    QuestionnaireId = questionnaire.Id,
                    UserId = user.Id,
                    QuestionId = Convert.ToInt32(key.Split('_')[1]),
                };
                unitOfWork.AnswerRepository.Add(currentAnswer);
            }
            unitOfWork.Save();

            return View(unitOfWork.AnswerRepository.GetAllWithRelated());
        }
    }
}