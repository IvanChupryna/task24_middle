﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Repositories;

namespace Task24_Middle.Controllers
{
    public class ArticleController : Controller
    {
        UnitOfWork unitOfWork;
        public ArticleController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ActionResult ShowArticles()
        {
            return View(unitOfWork.ArticleRepository.GetAll());
        }
    }
}