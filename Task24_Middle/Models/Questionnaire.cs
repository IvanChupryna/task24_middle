﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class Questionnaire : BaseEntity
    {
        public DateTime FilledDateTime { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}