﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class QuestionType : BaseEntity 
    {
        public string TypeName { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
    }
}