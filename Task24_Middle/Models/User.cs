﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}