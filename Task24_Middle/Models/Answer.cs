﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class Answer : BaseEntity
    {
        public int UserId { get; set; }
        public int QuestionId { get; set; }
        public int QuestionnaireId { get; set; }
        public string AnswerText { get; set; }
        public virtual User User { get; set; }
        public virtual Question Question { get; set; }
        public virtual Questionnaire Questionnaire { get; set; }
    }
}