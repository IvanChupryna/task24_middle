﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class Article : BaseEntity
    {
        public DateTime DateTimePosted { get; set; }
        public string Header { get; set; }
        public string SubHeader { get; set; }
        public string Text { get; set; }
    }
}