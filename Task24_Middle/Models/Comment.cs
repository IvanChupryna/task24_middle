﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class Comment : BaseEntity
    {
        public int UserId { get; set; }
        public DateTime DateTimeCommented { get; set; }
        public string Text { get; set; }
        public virtual User User{ get; set; }
    }
}