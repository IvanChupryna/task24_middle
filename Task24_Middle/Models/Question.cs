﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class Question : BaseEntity 
    {
        public string QuestionText { get; set; }
        public int QuestionTypeId { get; set; }
        public string AvailableAnswers { get; set; }
        public virtual QuestionType QuestionType { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}