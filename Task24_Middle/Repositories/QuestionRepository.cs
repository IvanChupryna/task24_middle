﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Task24_Middle.Models;
using Task24_Middle.Repositories.Abstract;

namespace Task24_Middle.Repositories
{
    public class QuestionRepository : Repository<Question>, IQuestionRepository
    {
        public QuestionRepository(NewsDbContext newsDbContext)
        {
            entities = newsDbContext.Questions;
        }

        public void Update(Question question)
        {
            Question questionToUpdate = GetById(question.Id);
            if(questionToUpdate != null)
            {
                questionToUpdate.Answers = question.Answers;
                questionToUpdate.AvailableAnswers = question.AvailableAnswers;
                questionToUpdate.QuestionText = question.QuestionText;
                questionToUpdate.QuestionType = question.QuestionType;
                questionToUpdate.QuestionTypeId = question.QuestionTypeId;
            }
        }

        public IEnumerable<Question> GetAllWithRelated()
        {
            return entities.Include(q => q.QuestionType).Include(q => q.Answers);
        }
    }
}