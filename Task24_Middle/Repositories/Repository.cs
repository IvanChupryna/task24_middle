﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Task24_Middle.Models;
using Task24_Middle.Repositories.Abstract;

namespace Task24_Middle.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected DbSet<TEntity> entities;

        public void Add(TEntity entity)
        {
            entities.Add(entity);
        }

        public IQueryable<TEntity> GetAll()
        {
            return entities;
        }

        public TEntity GetById(int id)
        {
            return entities.Find(id);
        }

        public void Remove(TEntity entity)
        {
            entities.Remove(entity);
        }
    }
}