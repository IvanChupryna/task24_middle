﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Task24_Middle.Models;
using Task24_Middle.Repositories.Abstract;

namespace Task24_Middle.Repositories
{
    public class AnswerRepository : Repository<Answer>, IAnswerRepository
    {
        public AnswerRepository(NewsDbContext newsDbContext) 
        {
            entities = newsDbContext.Answers;
        }

        public IEnumerable<Answer> GetAllWithRelated()
        {
            return entities.Include(a => a.Question).Include(a => a.Questionnaire).Include(a => a.User);
        }
    }
}