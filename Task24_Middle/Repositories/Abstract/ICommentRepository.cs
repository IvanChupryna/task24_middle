﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Middle.Models;

namespace Task24_Middle.Repositories.Abstract
{
    public interface ICommentRepository : IRepository<Comment>
    {
        void Update(Comment comment);
        IEnumerable<Comment> GetAllWithRelated();
    }
}
