﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task24_Middle.Models;

namespace Task24_Middle.Repositories.Abstract
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByName(string name);
        void Update(User user);
    }
}
