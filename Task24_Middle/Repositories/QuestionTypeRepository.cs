﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Middle.Models;
using Task24_Middle.Repositories.Abstract;

namespace Task24_Middle.Repositories
{
    public class QuestionTypeRepository : Repository<QuestionType>, IQuestionTypeRepository
    {
        public QuestionTypeRepository(NewsDbContext newsDbContext)
        {
            entities = newsDbContext.QuestionTypes;
        }
        public void Update(QuestionType questionType)
        {
            QuestionType questionTypeToUpdate = GetById(questionType.Id);
            if(questionTypeToUpdate != null)
            {
                questionTypeToUpdate.Questions = questionType.Questions;
                questionTypeToUpdate.TypeName = questionType.TypeName;
            }
        }
    }
}