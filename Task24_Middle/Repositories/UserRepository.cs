﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Middle.Models;
using Task24_Middle.Repositories.Abstract;

namespace Task24_Middle.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(NewsDbContext newsDbContext)
        {
            entities = newsDbContext.Users;
        }
        public void Update(User user)
        {
            User userToUpdate = GetById(user.Id);
            if(userToUpdate != null)
            {
                userToUpdate.Name = user.Name;
                userToUpdate.Answers = user.Answers;
                userToUpdate.Comments = user.Comments;
            }
        }

        public User GetByName(string name)
        {
            return entities.FirstOrDefault(u => u.Name == name);
        }
    }
}