﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Middle.Repositories.Abstract;

namespace Task24_Middle.Repositories
{
    public class UnitOfWork : IDisposable
    {
        NewsDbContext _newsDb;
        IAnswerRepository _answerRepository;
        IArticleRepository _articleRepository;
        ICommentRepository _commentRepository;
        IQuestionnaireRepository _questionnaireRepository;
        IQuestionRepository _questionRepository;
        IQuestionTypeRepository _questionTypeRepository;
        IUserRepository _userRepository;

        public UnitOfWork()
        {
            _newsDb = new NewsDbContext();
        }

        public IAnswerRepository AnswerRepository
        {
            get
            {
                if(_answerRepository == null)
                {
                    _answerRepository = new AnswerRepository(_newsDb);
                }
                return _answerRepository;
            }
        }

        public IArticleRepository ArticleRepository
        {
            get
            {
                if (_articleRepository == null)
                {
                    _articleRepository = new ArticleRepository(_newsDb);
                }
                return _articleRepository;
            }
        }

        public ICommentRepository CommentRepository
        {
            get
            {
                if (_commentRepository == null)
                {
                    _commentRepository = new CommentRepository(_newsDb);
                }
                return _commentRepository;
            }
        }

        public IQuestionRepository QuestionRepository
        {
            get
            {
                if (_questionRepository == null)
                {
                    _questionRepository = new QuestionRepository(_newsDb);
                }
                return _questionRepository;
            }
        }

        public IQuestionnaireRepository QuestionnaireRepository
        {
            get
            {
                if (_questionnaireRepository == null)
                {
                    _questionnaireRepository = new QuestionnaireRepository(_newsDb);
                }
                return _questionnaireRepository;
            }
        }

        public IQuestionTypeRepository QuestionTypeRepository
        {
            get
            {
                if (_questionTypeRepository == null)
                {
                    _questionTypeRepository = new QuestionTypeRepository(_newsDb);
                }
                return _questionTypeRepository;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_newsDb);
                }
                return _userRepository;
            }
        }

        public void Save()
        {
            _newsDb.SaveChanges();
        }

        public void Dispose()
        {
            _newsDb.Dispose();
        }
    }
}