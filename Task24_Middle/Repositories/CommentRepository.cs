﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Task24_Middle.Models;
using Task24_Middle.Repositories.Abstract;

namespace Task24_Middle.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(NewsDbContext newsDbContext)
        {
            entities = newsDbContext.Comments;
        }
        public void Update(Comment comment)
        {
            Comment commentToUpdate = GetById(comment.Id);
            if(commentToUpdate != null)
            {
                commentToUpdate.Text = comment.Text;
                commentToUpdate.UserId = comment.UserId;
                commentToUpdate.DateTimeCommented = comment.DateTimeCommented;
                commentToUpdate.User = comment.User;
            }
        }
        public IEnumerable<Comment> GetAllWithRelated()
        {
            return entities.Include(c => c.User);
        }
    }
}