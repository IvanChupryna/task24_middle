﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Middle.Models;
using Task24_Middle.Repositories.Abstract;

namespace Task24_Middle.Repositories
{
    public class ArticleRepository : Repository<Article>, IArticleRepository
    {
        public ArticleRepository(NewsDbContext newsDbContext)
        {
            entities = newsDbContext.Articles;
        }
        public void Update(Article article)
        {
            Article articleToUpdate = GetById(article.Id);
            if(articleToUpdate == null)
            {
                articleToUpdate.Header = article.Header;
                articleToUpdate.SubHeader = article.SubHeader;
                articleToUpdate.Text = article.Text;
                articleToUpdate.DateTimePosted = article.DateTimePosted;
            }
        }
    }
}