﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Task24_Middle.DbInitializers;
using Task24_Middle.Models;

namespace Task24_Middle
{
    public class NewsDbContext : DbContext
    {
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public NewsDbContext() : base("NewsContext")
        {
            Database.SetInitializer(new NewsInitializer());
        }

    }
}